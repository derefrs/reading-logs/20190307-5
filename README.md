# 20190307 - Deref.rs \#5

Thu, 07. Mar. 2019

https://derefrs.connpass.com/event/122511/

```toml
[meetup]
name = "Deref Rust"
date = "20190307"
version = "0.0.5"
attendees = []
repository = "https://gitlab.com/derefrs/reading-logs/20190307-5"
keywords = ["Rust"]

[locations]
park6 = {site = "Roppongi"}
zulip = {site = "https://derefrs.zulipchat.com", optional = true }
```

## Notes

| Name | Snippet / Note / What I did in few words |
|--|--|
|||


## Links

* [Deref Rust - GitLab.com](https://gitlab.com/derefrs)
* [Deref Rust - Zulip Chat](https://derefrs.zulipchat.com/)

## License

`MIT`

```text
Reading Logs
Copyright (c) 2019 Deref.rs

This is free software: You can redistribute it and/or modify
it under the terms of the MIT License.
```
